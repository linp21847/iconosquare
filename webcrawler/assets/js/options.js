var displayAlert = function(option, text) {
		$('#status').html('<p class="alert alert-' + option + '">' + text + '</p>');
			setTimeout(function () {
				$('#status').html('');
			}, 3500);
	};

var save_options = function() {
		TagCashBot.saveState("maxPage", $("#maxpagecount").val());

		displayAlert('success', 'Options saved.');
	};

var restore_options = function() {
		var maxPage = TagCashBot.getState().maxPage;

		if (maxPage) {
			$("#maxpagecount").val(maxPage);
		}

		$('#btnsave').click(save_options);
	};

var initTab = function () {
		$("ul.nav-tabs li.tab").click(function() {
			$("ul.nav-tabs li").removeClass("active");
			$(this).addClass("active");
			$("#database div.table").addClass("hidden");
			$("#database div.table#" + $(this).attr("table")).removeClass("hidden");

			if ($(this).attr('table') == 'leads') {
				$("ul.nav ul.dropdown-menu li#remove a").addClass('disabled');
			} else {
				$("ul.nav ul.dropdown-menu li#remove a").removeClass('disabled');
			}
		});

		$(".disabled").click(function(event) {
			event.preventDefault();
			return;
		});

		$("ul.dropdown-menu li#refresh a").click(function() {
			if ( $("ul.nav-tabs li.tab.active").attr("table") == "leads" ) {
				displayLeads(function() {
					displayAlert('success', 'Leads table is refreshed successfully.');
				});
			} else {
				displayContacts(function() {
					displayAlert('success', 'Contacts table is refreshed successfully.');
				});
			}
		});

		$("ul.dropdown-menu li#remove a").click(function() {
			if ( $("ul.nav-tabs li.tab.active").attr("table") == "leads" ) {
				console.log("You are not able to remove content from lead table.");
				displayAlert("danger", "You are not able to remove content from lead table.");
			} else {
				if (confirm("Are you sure to delete all contacts from database?")) {
					TagCashAPI.deleteAllContacts(function(res) {
						if (res) {
							displayAlert("success", "Contacts are deleted from database");
							displayContacts();
						}
					})
				}
			}
		});

		if ( $("ul.nav-tabs li.active").length == 0 ) {
			$nav = $($("ul.nav-tabs li")[0]);
			$nav.addClass("active")
			$("#database div.table").addClass("hidden");
			$("#database div.table#" + $nav.attr("table")).removeClass("hidden");
		};

		displayLeads();
		displayContacts();
	};

var displayLeads = function(callback) {
		$leadsContainer = $("div#leads table tbody");
		$leadsContainer.find("tr").remove();

		TagCashAPI.getAllLeads(function(leads) {
			for (var i = 0; i < leads.length; i++ ) {
				var curLead = leads[i];

				$leadsContainer.append($('<tr/>').append(
						$('<td/>').text(curLead[0]),
						$('<td/>').text(curLead[1]),
						$('<td/>').text(curLead[2]),
						$('<td/>').text(curLead[4]),
						$('<td/>').text(curLead[3]),
						$('<td/>').text(curLead[5]),
						$('<td/>').text(curLead[6]),
						$('<td/>').append($('<input class="preserve" prev="' + curLead[7] + '" placeholder="full if you want full search" data-id="' + curLead[0] + '" value="' + curLead[7] + '" />'))
					));
			}

			//	Event listener for preserve field in lead table.
			$(document).ready(function() {
				$('div#leads table tbody tr input.preserve').change(function() {
					var self = $(this);
					if (confirm("Are you sure to edit this value?")) {
						TagCashAPI.updateLeadPreserve(self.attr('data-id'), self.val(), function() {
							displayAlert('success', "A record was updated successfully.");
						});
					} else {
						self.val(self.attr('prev'));
					}
				});
			});

			if (typeof callback == "function") {
				callback();
			}
		})
	};

var displayContacts = function(callback) {
		$contactsContainer = $("div#contacts table tbody");
		$contactsContainer.find("tr").remove();
		TagCashAPI.getAllContacts(function(contacts) {
			for (var i = 0; i < contacts.length; i++ ) {
				var cur = contacts[i];
				$contactsContainer.append($('<tr/>').append(
						$('<td/>').text(cur[0]),	//	ID
						$('<td/>').append($('<a/>', {href: cur[1]}).text(truncText(cur[1], 60, "..."))),	//	Source network
						$('<td/>').text(cur[2]),	//	email
						$('<td/>').text(cur[3]),	//	Instagram
						$('<td/>').text(cur[4]),	//	Facebook
						$('<td/>').text(cur[5]),	//	Twitter
						$('<td/>').text(cur[6]),	//	Retailer
						$('<td/>').text(cur[8]),	//	Country
						$('<td/>').text(cur[7])		//	Note
					));
			}
			if (typeof callback == "function")
				callback();
		})
	};

var initializeOptionPage = function() {
		restore_options();
		initTab();
	};

$(document).ready(function () {
    initializeOptionPage();
});