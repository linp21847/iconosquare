(function(window, jQuery){

    $(document).ready(function(){

    	chrome.extension.sendMessage({
    		msg: 'status'
    	}, function(response) {
    		if (response.status == "started") {
    			if (response.botMode) {
    				$("div.retailer-search").hide();
    				$("div.bot").removeClass("hidden");
    				$("#stop").removeClass("hidden");
    			} else {
    				$("div.bot").hide();
    				$("div.retailer-search").removeClass("hidden");
    				$("#search-stop").removeClass("hidden");
    			}
    		} else {
    			$("div.bot").removeClass("hidden");
    			$("div.bot #start").removeClass("hidden");
                // $("div.bot #restart").removeClass("hidden");
    			$("div.retailer-search").removeClass("hidden");
                $("div.retailer-search #retailer").removeClass("hidden");
                $("div.retailer-search #source").removeClass("hidden");
    			$("div.retailer-search #search-start").removeClass("hidden");
                $("div.retailer-search #search-restart").removeClass("hidden");
    		}

            if (response.partial_lead)
                $('#retailer').val(response.partial_lead.name);

            if (response.partial_source)
                $('#source').val(response.partial_source);

            if ((response.contacts) && (response.contacts.length > 0)) {
                $("div.retailer-search #download span.badge").text(response.contacts.length);
                $("div.retailer-search #download").removeClass("hidden");
            }

            if ((response.bot_contacts) && (response.bot_contacts.length > 0)) {
                $("#bot-download span.badge").text(response.bot_contacts.length);
                $("div.bot #bot-download").removeClass("hidden");
            }
    	});

        $("#start").click(function(event) {
            event.preventDefault();
            chrome.extension.sendMessage({msg: "bot-start"}, function(response) {
                console.log("Response from background script: " + response.data);
                if (response.data === "Started") {
                    $("#start").hide();
                    // $("#restart").hide();
                    $("#stop").removeClass("hidden");
                } else if (response.data == "no_lead") {
                    $("#stop").hide();
                    $("div.bot").removeClass("hidden");
                    $("div.bot #start").removeClass("hidden");
                    // $("div.bot #restart").removeClass("hidden");
                    $("div.retailer-search").removeClass("hidden");
                    $("div.retailer-search #search-start").removeClass("hidden");
                    $("div.retailer-search #search-restart").removeClass("hidden");
                }
            });
        });

        // $("#restart").click(function(event) {
        //     event.preventDefault();
        //     chrome.extension.sendMessage({msg: "bot-restart"}, function(response) {
        //         console.log("Response from background script: " + response.data);
        //         if (response.data === "Started") {
        //             $("#start").hide();
        //             $("#restart").hide();
        //             $("#stop").removeClass("hidden");
        //         } else if (response.data == "no_lead") {
        //             $("#stop").hide();
        //             $("div.bot").removeClass("hidden");
        //             $("div.bot #start").removeClass("hidden");
        //             $("div.bot #restart").removeClass("hidden");
        //             $("div.retailer-search").removeClass("hidden");
        //             $("div.retailer-search #search-start").removeClass("hidden");
        //             $("div.retailer-search #search-restart").removeClass("hidden");
        //         }
        //     });
        // });

        $("#stop").click(function(event) {
            event.preventDefault();
            chrome.extension.sendMessage({msg: "bot-stop"}, function(response) {
                console.log("Response from background script: " + response.data);
                $("#stop").hide();
                $("div.bot").removeClass("hidden");
    			$("div.bot #start").removeClass("hidden");
                // $("div.bot #restart").removeClass("hidden");
    			$("div.retailer-search").removeClass("hidden");
    			$("div.retailer-search #search-start").removeClass("hidden");
                $("div.retailer-search #search-restart").removeClass("hidden");

    			chrome.extension.sendMessage({
						msg: 'status'
					}, function(response) {
						if ((response.contacts) && (response.contacts.length > 0)) {
							$("div.retailer-search #download").removeClass("hidden");
						}
					});
            });
        });

        $("#search-start").click(function(event) {
        	var lead = $('#retailer').val(),
        		source = $("#source").val();
        	event.preventDefault();

        	if (!lead) {
        		alert("Input lead name.", function() {
        			$("#retailer").focus();
        		});
        	} else if (!source) {
        		alert("Source network should be selected.");
        	} else {
        		chrome.extension.sendMessage({msg: "search-start", retailer: lead, source_network: source}, function(response) {
	                console.log("Response from background script: " + response.data);

                    if (response.status == "started") {
                        $("div.bot").addClass("hidden");
                        $("#search-start").addClass("hidden");
                        $("#search-restart").addClass("hidden");
                        $("div.retailer-search #retailer").addClass("hidden");
                        $("div.retailer-search #source").addClass("hidden");
                        $("#search-stop").removeClass("hidden");
                    } else if (response.status == "stopped") {
                        alert(response.msg);
                    }
	            });
        	}
        });

        $("#search-restart").click(function(event) {
            event.preventDefault();
            var lead = $('#retailer').val(),
                source = $("#source").val();

            if (!lead) {
                alert("Input lead name.", function() {
                    $("#retailer").focus();
                });
            } else if (!source) {
                alert("Source network should be selected.");
            } else {
                chrome.extension.sendMessage({msg: "search-restart", retailer: lead, source_network: source}, function(response) {
                    console.log("Response from background script: " + response.data);
                    if (response.data === "Started") {
                        $("div.bot").addClass("hidden");
                        $("#search-start").addClass("hidden");
                        $("#search-restart").addClass("hidden");
                        $("div.retailer-search #retailer").addClass("hidden");
                        $("div.retailer-search #source").addClass("hidden");
                        $("#search-stop").removeClass("hidden");
                    } else if (response.data == "no_lead") {
                        $("#stop").hide();
                        $("div.bot").removeClass("hidden");
                        $("div.bot #start").removeClass("hidden");
                        // $("div.bot #restart").removeClass("hidden");
                        $("div.retailer-search").removeClass("hidden");
                        $("div.retailer-search #search-start").removeClass("hidden");
                        $("div.retailer-search #search-restart").removeClass("hidden");
                    }
                    // console.log("Response from background script: " + response.data);
                    // $("#search-start").hide();
                    // $("#search-stop").removeClass("hidden");
                });
            }
        });

        $("#search-stop").click(function(event) {
            event.preventDefault();
            chrome.extension.sendMessage({msg: "search-stop"}, function(response) {
                console.log("Response from background script: " + response.data);

                $("div.bot").removeClass("hidden");
                $("#search-start").removeClass("hidden");
                $("#search-restart").removeClass("hidden");
                $("div.retailer-search #retailer").removeClass("hidden");
                $("div.retailer-search #source").removeClass("hidden");
                $("#search-stop").addClass("hidden");

    			chrome.extension.sendMessage({
						msg: 'status'
					}, function(response) {
						if ((response.contacts) && (response.contacts.length > 0)) {
							$("div.retailer-search #download").removeClass("hidden");
						}
					});
            });
        });
        var x = function(flag, obj){
                return function(){
                    downloadContacts(flag, obj);
                }
        };

        $("#download").click(x("non-bot", $("#download")));
        $("#bot-download").click(x("bot", $("#bot-download")));
    });


})(window, $);