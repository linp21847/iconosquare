var TagCashAPI = {
	// base: "http://tagcash.dev:8080/api/",
	base: "http://front-end.tagcash.tv/api/",
	getLead: function(callback) {
		if (typeof callback == "function") {
			$.ajax({
					url: this.base + 'get_lead',
					type: "GET",
					dataType: "json",
					success: function(res) {
						callback(res);
					},
					error: function(x, e) {
						console.log(e);
					}
				});
		} else {
			console.log("Callback function is required..");
			// alert("Callback function needed.");
		}
	},

	getAllLeads: function(callback) {
		if (typeof callback == "function") {
			$.ajax({
					url: this.base + 'get_leads',
					type: "GET",
					dataType: "json",
					success: function(res) {
						callback(res);
					},
					error: function(x, e) {
						console.log(e);
					}
				});
		} else {
			console.log("Callback function is required..");
		}
	},

	getAllContacts: function (callback) {
		if (typeof callback == "function") {
			$.ajax({
					url: this.base + 'get_contacts',
					type: "GET",
					dataType: "json",
					success: function(res) {
						callback(res);
					},
					error: function(x, e) {
						console.log(e);
					}
				});
		} else {
			console.log("Callback function is required..");
		}
	},

	deleteAllContacts: function(callback) {
		if (typeof callback == "function") {
			$.ajax({
					url: this.base + 'delete_contacts',
					type: "GET",
					dataType: "json",
					success: function(res) {
						callback(res);
					},
					error: function(x, e) {
						console.log(e);
					}
				});
		} else {
			console.log("Callback function is required..");
		}
	},

	markLeadAsFull: function(id, callback) {
		TagCashAPI.updateLeadPreserve(id, 'full', callback);
	},

	updateLeadPreserve: function(id, preserve, callback) {
		$.ajax({
			url: this.base + 'update_lead',
			type: "GET",
			dataType: "json",
			data: {
				id: id,
				preserve: preserve
			},
			success: function(res) {
				if (typeof callback == "function") {
					callback(res);
				} else {
					console.log(res);
				}
			}
		});
	},

	markLeadAsComplete: function(lead, contact, callback) {
		$.ajax({
			url: this.base + 'update_lead',
			type: "GET",
			dataType: "json",
			data: {
				id: lead.id,
				pose: lead.pose,
				lookbook: lead.lookbook,
				chictopia: lead.chictopia,
				preserve: 'completed'
			},
			success: function(res) {
				if (typeof callback == "function") {
					callback(res);
				} else {
					console.log(res);
				}
			}
		});
	},

	addContact: function(lead, contact, callback) {
		$.ajax({
			url: this.base + 'add_contact',
			type: "GET",
			dataType: "json",
			data: {
				source_network: contact.source_network,
				email: "Not found",//contact.email,
				instagram: contact.instagram_count,
				facebook: contact.facebook_count,
				twitter: contact.twitter_count,
				retailer: lead.name,
				note: contact.note,
				country: contact.country
			},
			success: function(res) {
				if (typeof callback == "function") {
					callback(res);
				} else {
					console.log(res);
				}
			}
		});
	},

	addComplete: function(lead, contact, callback) {
		$.ajax({
			url: this.base + 'add_complete',
			type: "GET",
			dataType: "json",
			data: {
				source_network: contact.source_network,
				email: contact.email,
				main_network: contact.blog,
				instagram: contact.instagram_count,
				facebook: contact.facebook_count,
				twitter: contact.twitter_count,
				retailer: lead.name,
				note: contact.note,
				country: contact.country
			},
			success: function(res) {
				if (typeof callback == "function") {
					callback(res);
				} else {
					console.log(res);
				}
			}
		});
	},

	deleteLead: function(lead, callback) {
		$.ajax({
			url: this.base + 'delete_lead',
			type: "GET",
			dataType: "json",
			data: {
				id: lead.id
			},
			success: function(res) {
				if (typeof callback == "function") {
					callback(res);
				} else {
					console.log(res);
				}
			}
		});
	}
};

var TagCashBot = {
	botMode: true,
	tabs: {},
	timer: null,
	facebook_timer: null,
	twitter_timer: null,
	instagram_timer: null,
	blog_timer: null,
	lookbook: null,
	lookbook_timer: null,
	lookbook_tab_id: null,
	pose: null,
	pose_timer: null,
	pose_tab_id: null,
	chictopia: null,
	chictopia_timer: null,
	chictopia_tab_id: null,
	source_network_tab_id: null,
	source_network_url: null,
	instagram_tab_id: null,
	facebook_tab_id: null,
	twitter_tab_id: null,
	blog_tab_id: null,
	contact: null,
	complete: null,

	saveContact: function(callback) {
		contacts = TagCashBot.getState().contacts || [];
		lead = TagCashBot.getState().lead;
		TagCashBot.contact.retailer = lead.name;
		var contact = TagCashBot.contact,
			isExist = isExistInContacts(contact, contacts);

		if (isExist.status) {
			contacts[isExist.index].note = (contacts[isExist.index].note || 1) + 1;
			if (TagCashBot.source_network_url.indexOf("chictopia.com/") > -1) {
				lead.chictopia++;
			} else if (TagCashBot.source_network_url.indexOf("pose.com/") > -1) {
				lead.pose++;
			} else if (TagCashBot.source_network_url.indexOf("lookbook.nu/") > -1) {
				lead.lookbook++;
			}
			TagCashBot.saveState("lead", lead);
			TagCashBot.saveState("contacts", contacts);
			TagCashBot.contact = {};
			if (typeof callback == "function") {
				callback();
			}
		} else if (TagCashBot.getState().botMode) {
			TagCashAPI.addContact(lead, contact, function(ret) {
					if (ret.status == "ok") {
						TagCashAPI.addComplete(lead, contact, function(res) {
								console.log(res);
							});

						if (TagCashBot.source_network_url.indexOf("chictopia.com/") > -1) {
							lead.chictopia++;
						} else if (TagCashBot.source_network_url.indexOf("pose.com/") > -1) {
							lead.pose++;
						} else if (TagCashBot.source_network_url.indexOf("lookbook.nu/") > -1) {
							lead.lookbook++;
						}
						TagCashBot.saveState("lead", lead);
						contacts.push(contact);
						TagCashBot.saveState("contacts", contacts);
						TagCashBot.contact = {};
					} else {
						console.log("Adding via API failed.")
					}

					if (typeof callback == "function") {
						callback();
					}
				});
		} else {
			if (TagCashBot.source_network_url.indexOf("chictopia.com/") > -1) {
				lead.chictopia++;
			} else if (TagCashBot.source_network_url.indexOf("pose.com/") > -1) {
				lead.pose++;
			} else if (TagCashBot.source_network_url.indexOf("lookbook.nu/") > -1) {
				lead.lookbook++;
			}
			TagCashBot.saveState("lead", lead);
			contacts.push(TagCashBot.contact);
			TagCashBot.saveState("contacts", contacts);
			TagCashBot.contact = {};

			if (typeof callback == "function") {
				callback();
			}
		}
	},


	/**
	 *	Lookbook site control methods
	 */

		checkLeadOnLookbook: function(name, lookbook) {
			if (!lookbook) {
				TagCashBot.saveState("lookbooks", []);
				chrome.tabs.create({url: 'http://lookbook.nu/search?q=' + name}, function(tab) {
					console.log(tab);
					TagCashBot.lookbook_tab_id = tab.id;
				});
			} else if (lookbook == "done") {
				TagCashBot.processLead()
			} else {
				TagCashBot.resumeLookbookItem();
			}
		},

		addLookbookItems: function(urls) {
			var backup = TagCashBot.getState().lookbooks;

			if (!TagCashBot.getState().lookbook && TagCashBot.getState().lookbooks.length > 0 && !TagCashBot.lookbook_timer) {
				TagCashBot.saveState("lookbooks", []);
			}

			TagCashBot.saveState("lookbooks", backup.concat(urls));

			if (!TagCashBot.lookbook_timer) {
				TagCashBot.lookbook_timer = setTimeout(function() {
					console.log("Checking poing: " + TagCashBot.getState().lookbook);
					if (!TagCashBot.getState().lookbook) {
						TagCashBot.processLookbookItem("Bot is starting to process a lead on lookbook...");
					}
					clearTimeout(TagCashBot.lookbook_timer);
				}, 100);
			}
		},

		processLookbookItem: function(log) {
			console.log("processLookbookItem says. " + log);
			TagCashBot.initVariables();
			var backup = TagCashBot.getState().lookbooks;
			var url = backup.shift();
			if (!url) {
				TagCashBot.saveState('lookbook', "done");
				TagCashBot.saveState('TagCashBot.source_network_url', "");
				TagCashBot.source_network_url = null;
				if (TagCashBot.lookbook_tab_id){
					removeChromeTab(TagCashBot.lookbook_tab_id, function() {
						//TagCashBot.lookbook_tab_id = null;
						console.log("Lookbook search page tab closed.");
					});
				}
				TagCashBot.processLead();
			} else {

				TagCashBot.saveState('lookbooks', backup);
				TagCashBot.saveState('lookbook', url);
				
				chrome.tabs.create({url: url}, function(tab) {
					TagCashBot.source_network_tab_id = tab.id;
					TagCashBot.source_network_url = tab.url;
					TagCashBot.contact = {};
					TagCashBot.contact.source_network = url;
					TagCashBot.saveState("source_network", url);
					TagCashBot.complete = {};
				});
			}
		},

		resumeLookbookItem: function(log) {
			console.log("resumeLookbookItem says. " + log);
			TagCashBot.initVariables();
			var url = TagCashBot.getState().lookbook;
			if (url) {
				
				chrome.tabs.create({url: url}, function(tab) {
					TagCashBot.source_network_tab_id = tab.id;
					TagCashBot.source_network_url = tab.url;
					TagCashBot.contact = {};
					TagCashBot.contact.source_network = url;
					TagCashBot.saveState("source_network", url);
					TagCashBot.complete = {};
				});
			}
		},
	/*	End of Lookbook site control methods	*/

	/**
	 *	Pose site control methods
	 */

		checkLeadOnPose: function(name, pose) {

			if (!pose) {
				TagCashBot.saveState("poses", []);
				chrome.tabs.create({url: 'https://pose.com/search/poses?keyword=' + name}, function(tab) {
					console.log(tab);
					TagCashBot.pose_tab_id = tab.id;
				});
			} else if (pose == "done") {
				TagCashBot.processLead()
			} else {
				TagCashBot.resumePoseItem();
			}
		},


		addPoseItems: function(urls) {
			var backup = TagCashBot.getState().poses;

			if (!TagCashBot.getState().pose && TagCashBot.getState().poses.length > 0 && !TagCashBot.pose_timer) {
				TagCashBot.saveState('poses', []);
			}

			backup = backup.concat(urls);
			TagCashBot.saveState("poses", backup);

			if (!TagCashBot.pose_timer) {
				TagCashBot.pose_timer = setTimeout(function() {
					console.log("Checking poing: " + TagCashBot.getState().pose);
					if (!TagCashBot.getState().pose)
						TagCashBot.processPoseItem("Bot is starting to process a lead on pose...");
					clearTimeout(TagCashBot.pose_timer);
				}, 100);
			}
		},

		processPoseItem: function(log) {
			console.log("processPoseItem says. " + log);
			TagCashBot.initVariables();
			var backup = TagCashBot.getState().poses;
			var url = backup.shift();
			if (!url) {
				TagCashBot.saveState('pose', "done");
				TagCashBot.saveState('source_network_url', "");
				TagCashBot.source_network_url = null;
				if (TagCashBot.pose_tab_id){
					removeChromeTab(TagCashBot.pose_tab_id, function() {
						//TagCashBot.pose_tab_id = null
						console.log("Pose search page tab closed.");
					});
				}
				TagCashBot.processLead();
			} else {

				TagCashBot.saveState('poses', backup);
				TagCashBot.saveState('pose', url);
				
				chrome.tabs.create({url: url.profileUrl}, function(tab) {
					TagCashBot.source_network_tab_id = tab.id;
					TagCashBot.source_network_url = tab.url;
					TagCashBot.contact = {};
					TagCashBot.contact.source_network = url.imageUrl;
					TagCashBot.saveState("source_network", url);
					TagCashBot.complete = {};
				});
			}
		},

		resumePoseItem: function(log) {
			console.log("resumePoseItem says. " + log);
			TagCashBot.initVariables();
			var url = TagCashBot.getState().pose;
			if (url) {
				
				chrome.tabs.create({url: url.profileUrl}, function(tab) {
					TagCashBot.source_network_tab_id = tab.id;
					TagCashBot.source_network_url = tab.url;
					TagCashBot.contact = {};
					TagCashBot.contact.source_network = url.imageUrl;
					TagCashBot.saveState("source_network", url.profileUrl);
					TagCashBot.complete = {};
				});
			}
		},
	/*	End of Pose site control methods	*/


	/**
	 *	Chictopia control methods
	 */

		checkLeadOnChictopia: function(name, chictopia) {
			
			if (!chictopia) {
				TagCashBot.saveState("chictopias", []);
				chrome.tabs.create({url: 'http://www.chictopia.com/' + name + '/info'}, function(tab) {
					console.log(tab);
					TagCashBot.chictopia_tab_id = tab.id;
				});
			} else if (chictopia == "done") {
				TagCashBot.processLead()
			} else {
				TagCashBot.resumeChictopiaItem();
			}
		},

		addChictopiaItems: function(urls) {
			var backup = TagCashBot.getState().chictopias;

			if (!TagCashBot.getState().chictopia && TagCashBot.getState().chictopias.length > 0 && !TagCashBot.chictopia_timer) {
				TagCashBot.saveState('chictopias', []);
			}

			backup = backup.concat(urls);
			TagCashBot.saveState("chictopias", backup);

			if (!TagCashBot.chictopia_timer) {
				TagCashBot.chictopia_timer = setTimeout(function() {
					console.log("Checking poing: " + TagCashBot.getState().chictopia);
					if (!TagCashBot.getState().chictopia)
						TagCashBot.processChictopiaItem("Bot is starting to process a lead on pose...");
					clearTimeout(TagCashBot.chictopia_timer);
				}, 100);
			}
		},

		processChictopiaItem: function(log) {
			console.log("processChictopiaItem says. " + log);
			TagCashBot.initVariables();
			var backup = TagCashBot.getState().chictopias,
				url = backup.shift();
			if (!url) {
				TagCashBot.saveState('chictopia', "done");
				TagCashBot.saveState('TagCashBot.source_network_url', "");
				TagCashBot.source_network_url = null;
				TagCashBot.nextLead();
				if (TagCashBot.chictopia_tab_id){
					removeChromeTab(TagCashBot.chictopia_tab_id, function() {
						//TagCashBot.chictopia_tab_id = null
						console.log("Chictopia search page tab closed.");
					});
				}
				TagCashBot.processLead();
			} else {

				TagCashBot.saveState('chictopias', backup);
				TagCashBot.saveState('chictopia', url);
				
				chrome.tabs.create({url: url}, function(tab) {
					TagCashBot.source_network_tab_id = tab.id;
					TagCashBot.source_network_url = tab.url;
					TagCashBot.contact = {};
					TagCashBot.contact.source_network = url;
					TagCashBot.saveState("source_network", url);
					TagCashBot.complete = {};
				});
			}
		},

		resumeChictopiaItem: function(log) {
			console.log("resumeChictopiaItem says. " + log);
			TagCashBot.initVariables();
			var url = TagCashBot.getState().chictopia;
			if (url) {
				
				chrome.tabs.create({url: url}, function(tab) {
					TagCashBot.source_network_tab_id = tab.id;
					TagCashBot.source_network_url = tab.url;
					TagCashBot.contact = {};
					TagCashBot.contact.source_network = url;
					TagCashBot.saveState("source_network", url);
					TagCashBot.complete = {};
				});
			}
		},
	/*	End of Chictopia control methods	*/

	/**
	 *	Social site search
	 */
	 	visitSocialSites: function() {
			//
			var flag = true;
			if (contact.instagram) {
				flag = false;
				TagCashBot.contact.instagram = removeParams(TagCashBot.contact.instagram);
				TagCashBot.visitInstagram();
			} else {
				contact.instagram = "Unknown";
				contact.instagram_count = "0";
			}

			if (contact.facebook) {
				flag = false;
				TagCashBot.contact.facebook = removeParams(TagCashBot.contact.facebook)
				TagCashBot.visitFacebook();
			} else {
				contact.facebook = "Unknown";
				contact.facebook_count = "0";
			}

			if (contact.twitter) {
				flag = false;
				TagCashBot.contact.twitter = removeParams(TagCashBot.contact.twitter);
				TagCashBot.visitTwitter();
			} else {
				contact.twitter = "Unknown";
				contact.twitter_count = "0";
			}

			if (flag) {
				TagCashBot.avoidExceptionalCase();
			}
		},

		visitFacebook: function(url) {
			chrome.tabs.create({url: TagCashBot.contact.facebook}, function(tab) {
				TagCashBot.facebook_tab_id = tab.id;
				TagCashBot.facebook_timer = setTimeout(function() {
					TagCashBot.contact.facebook_count = "0";
					if (tab.id) {
						removeChromeTab(tab.id, function() {
							//TagCashBot.facebook_tab_id = null;
						});
					}
					clearTimeout(TagCashBot.facebook_timer);
					if (TagCashBot.isReady()) {
						TagCashBot.avoidExceptionalCase();
					}
				}, 30000);
			});
		},

		visitTwitter: function() {
			chrome.tabs.create({url: removeParams(TagCashBot.contact.twitter)}, function(tab) {
				TagCashBot.twitter_tab_id = tab.id;
				TagCashBot.twitter_timer = setTimeout(function() {
					TagCashBot.contact.twitter_count = "0";
					if (tab.id) {
						removeChromeTab(tab.id, function() {
							//TagCashBot.twitter_tab_id = null;
						});
					}
					clearTimeout(TagCashBot.twitter_timer);
					if (TagCashBot.isReady()) {
						TagCashBot.avoidExceptionalCase();
					}
				}, 30000);
			});
		},

		visitInstagram: function() {
			chrome.tabs.create({url: removeParams(TagCashBot.contact.instagram)}, function(tab) {
				TagCashBot.instagram_tab_id = tab.id;
				TagCashBot.instagram_timer = setTimeout(function() {
					TagCashBot.contact.instagram_count = "0";
					if (tab.id) {
						removeChromeTab(tab.id, function() {
							//TagCashBot.instagram_tab_id = null;
						});
					}
					clearTimeout(TagCashBot.instagram_timer);
					if (TagCashBot.isReady()) {
						TagCashBot.avoidExceptionalCase();
					}
				}, 30000);
			});
		},
	/*	End of social site search features.	 */

	/**
	 *	Blog site control methods.
	 */

		visitBlog: function() {
			chrome.tabs.create({url: TagCashBot.contact.blog}, function(tab) {
				TagCashBot.blog_tab_id = tab.id;
				TagCashBot.blog_timer = setTimeout(function() {
					if (tab.id) {
						removeChromeTab(tab.id, function() {
							//TagCashBot.blog_tab_id = null;
						});
					}
					clearTimeout(TagCashBot.blog_timer);
					TagCashBot.visitSocialSites();
				}, 60000);
			});
		},

		scrapContactInfo: function() {
			//
			contact = TagCashBot.contact;
			if (!contact.blog) {
				TagCashBot.contact.blog = "Unknown";
				TagCashBot.visitSocialSites();
				TagCashBot.avoidExceptionalCase();
			}
			else if ( !(contact.instagram && contact.facebook && contact.twitter) && contact.blog ) {
				TagCashBot.visitBlog();
			} else {
				TagCashBot.visitSocialSites();
			}
		},
	/*	End of blog site control methods	*/


	/**
	 *	TagCashBot Controller methods.
	 */

		init: function() {
			TagCashBot.timer = 'InProgress';
		},

		initVariables: function() {

			if (TagCashBot.instagram_tab_id)
				removeChromeTab(TagCashBot.instagram_tab_id, function() {
					TagCashBot.instagram_tab_id = null;
				});

			if (TagCashBot.facebook_tab_id)
				removeChromeTab(TagCashBot.facebook_tab_id, function() {
					TagCashBot.facebook_tab_id = null;
				});

			if (TagCashBot.twitter_tab_id)
				removeChromeTab(TagCashBot.twitter_tab_id, function() {
					TagCashBot.twitter_tab_id = null;
				});

			if (TagCashBot.blog_tab_id)
				removeChromeTab(TagCashBot.blog_tab_id, function() {
					TagCashBot.blog_tab_id = null;
				});
			
			TagCashBot.contact = {};
			TagCashBot.complete = null;
		},

		isReady: function() {
			var contact = TagCashBot.contact;
			return (contact.facebook_count != null) && (contact.twitter_count != null) && (contact.instagram_count != null);
		},

		initState: function() {
			TagCashBot.saveState('lead', null);
			TagCashBot.lead == null;
			TagCashBot.saveState('lookbook', null);
			TagCashBot.lookbook = null;
			TagCashBot.lookbook_timer = null;
			TagCashBot.saveState('pose', null);
			TagCashBot.pose = null;
			TagCashBot.pose_timer = null;
			TagCashBot.saveState('chictopia', null);
			TagCashBot.chictopia = null;
			TagCashBot.chictopia_timer = null;
			TagCashBot.saveState('lookbooks', []);
			TagCashBot.saveState('poses', []);
			TagCashBot.saveState('chictopias', []);
			TagCashBot.saveState('contacts', []);
		},

		prepareIndividualSearch: function(source_option) {
			//
			TagCashBot.saveState("pose", "done");
			TagCashBot.saveState("poses", []);
			TagCashBot.saveState("chictopia", "done");
			TagCashBot.saveState("chictopias", []);
			TagCashBot.saveState("lookbook", "done");
			TagCashBot.saveState("lookbooks", []);
			TagCashBot.saveState("source_network", null);
			TagCashBot.lookbook_timer = null;
			TagCashBot.pose_timer = null;
			TagCashBot.chictopia_timer = null;

			if (source_option == "lookbook") {
				TagCashBot.saveState("lookbook", null);
			} 
			else if (source_option == "pose") {
				TagCashBot.saveState("pose", null);
			} 
			else if (source_option == "chictopia") {
				TagCashBot.saveState("chictopia", null);
			} 
			else {
				console.log("Unexpected source network is inputed. Please review.");
			}
		},

		startSearch: function(name, source_option, callback) {
			var lead = {
					name: name,
					source: "Linkshare",
					pose: "0",
					lookbook: "0",
					chictopia: "0",
					hashtag: null,
					preserve: null
				};
			var cur_option = TagCashBot.getState("partial_source"),
				cur_lead = TagCashBot.getState("lead");

			TagCashBot.saveState('botMode', false);
			// TagCashBot.prepareIndividualSearch(source_option);
			TagCashBot.saveState('partial_source', source_option);

					
			if (name == cur_lead.name) {
				if (source_option == "lookbook") {
					var lookbook = TagCashBot.getState().lookbook;
					if (lookbook == "done")
					{
						TagCashBot.saveState('status', 'stopped');
						TagCashBot.stop();
						if (typeof callback == "function") {
							callback({status: "stopped", msg: source_option + " was completed."});
						}
					} else {//if (lookbooks.length == 0) {
						// TagCashBot.saveState('lookbook', null);
						TagCashBot.saveState('status', 'started');
						TagCashBot.processLead(lead, null, source_option);
						if (typeof callback == "function") {
							callback({status: "started"});
						}
					}
				} else if (source_option == "pose") {
					var pose = TagCashBot.getState().pose;
					if (pose == "done")
					{
						TagCashBot.saveState('status', 'stopped');
						TagCashBot.stop();
						if (typeof callback == "function") {
							callback({status: "stopped", msg: source_option + " was completed."});
						}
					} else {//if (poses.length == 0) {
						// TagCashBot.saveState('pose', null);
						TagCashBot.saveState('status', 'started');
						TagCashBot.processLead(lead, null, source_option);
						if (typeof callback == "function") {
							callback({status: "started"});
						}
					}
				} else if (source_option == "chictopia") {
					var chictopia = TagCashBot.getState().chictopia;
					if (chictopia == "done")
					{
						TagCashBot.saveState('status', 'stopped');
						TagCashBot.stop();
						if (typeof callback == "function") {
							callback({status: "stopped", msg: source_option + " was completed."});
						}
					} else {//if (chictopias.length == 0) {
						// TagCashBot.saveState('chictopia', null);
						TagCashBot.saveState('status', 'started');
						TagCashBot.processLead(lead, null, source_option);
						if (typeof callback == "function") {
							callback({status: "started"});
						}
					}
				} else {
					console.log("Resumming with " + name + " on " + source_option);				
					TagCashBot.processLead(cur_lead, null, source_option);
				}
			} else {
				console.log("Starting with " + name + " on " + source_option);
				TagCashBot.saveState('lead', lead);
				TagCashBot.saveState('lookbook', null);
				TagCashBot.saveState('lookbooks', []);
				TagCashBot.saveState('pose', null);
				TagCashBot.saveState('poses', []);
				TagCashBot.saveState('chictopia', null);
				TagCashBot.saveState('chictopias', []);

				if (source_option == "lookbook") {
					var lookbooks = TagCashBot.getState().lookbooks;
					// if (lookbooks.length == 0 || lookbooks == "done")
					// {
						
						TagCashBot.processLead(lead, null, source_option);
					// }
				} else if (source_option == "pose") {
					var poses = TagCashBot.getState().poses;
					// if (poses.length == 0 || poses == "done")
					// {
						
						TagCashBot.processLead(lead, null, source_option);
					// }
				} else if (source_option == "chictopia") {
					var chictopias = TagCashBot.getState().chictopias;
					// if (chictopias.length == 0 || chictopias == "done")
					// {
						
						TagCashBot.processLead(lead, null, source_option);
					// }
				} else {
					TagCashBot.processLead(lead, null, source_option);
				}
			}
			TagCashBot.saveState('status', 'started');
			if (typeof callback == "function") {
				callback({status: "started"});
			}
		},

		restartSearch: function(name, source_option) {
			var lead = {
					name: name,
					source: "Linkshare",
					pose: "0",
					lookbook: "0",
					chictopia: "0",
					hashtag: null,
					preserve: null
				};

			TagCashBot.saveState('botMode', false);
			console.log("Restarting with " + name + " on " + source_option);
			TagCashBot.saveState('lead', lead);
			TagCashBot.saveState('partial_source', source_option || null);

			TagCashBot.prepareIndividualSearch(source_option);
			TagCashBot.saveState("contacts", []);

			TagCashBot.saveState('status', 'started');
			TagCashBot.processLead(lead);
		},

		restart: function(callback) {
			
			TagCashBot.stop();
			TagCashBot.initState();
			tempTimer = setTimeout(function() {
				TagCashBot.start();
				clearTimeout(tempTimer);
				if (typeof callback == "function")
					callback();
			}, 500);
		},

		start: function(callback) {
			var curState = TagCashBot.getState();
			TagCashBot.saveState('status', 'started');
			if (!(curState.lead)) {
				TagCashAPI.getLead(function(lead) {
					if (!lead) {
						alert("There is no lead to search.");

						if (typeof callback == "function") {
							callback({data: "no_lead"});
							TagCashBot.stop();
						} else {
							TagCashBot.stop();
						}
					} else {
						if (typeof callback == "function") {
							callback({data: "Started"});
						}
						lead.lookbook = 0;
						lead.pose = 0;
						lead.chictopia = 0;
						TagCashBot.lookbook_timer = null;
						TagCashBot.pose_timer = null;
						TagCashBot.chictopia_timer = null;
						TagCashBot.saveState("lead", lead);
						TagCashBot.saveState("lookbook", null);
						TagCashBot.saveState("pose", null);
						TagCashBot.saveState("chictopia", null);
						curState.lead = lead;
						TagCashBot.processLead(lead, curState);
					}
				})
			} else {
				callback({data: "Started"});
				TagCashBot.processLead(curState.lead, curState);
			}
		},

		getState: function(key) {
			if (!key) {
				if (TagCashBot.getState("botMode")) {
					return {
						lead: JSON.parse(localStorage.getItem('bot-lead')),
						pose: JSON.parse(localStorage.getItem('bot-pose')),
						poses: JSON.parse(localStorage.getItem('bot-poses')),
						lookbook: JSON.parse(localStorage.getItem('bot-lookbook')),
						lookbooks: JSON.parse(localStorage.getItem('bot-lookbooks')),
						chictopia: JSON.parse(localStorage.getItem('bot-chictopia')),
						chictopias: JSON.parse(localStorage.getItem('bot-chictopias')),
						status: ((TagCashBot.getState("lookbook") == "done") &&
								(TagCashBot.getState("pose") == "done") &&
								(TagCashBot.getState("chictopia") == "done")) ? "stopped" : JSON.parse(localStorage.getItem('status')),
						contacts: JSON.parse(localStorage.getItem('bot-contacts')) || [],
						botMode: JSON.parse(localStorage.getItem('botMode')) || false,
						// searchOption: JSON.parse(localStorage.getItem('bot-searchOption')) || 'lookbook',
						maxPage: JSON.parse(localStorage.getItem('maxPage')) || 1,
						partial_lead: JSON.parse(localStorage.getItem('lead')),
						partial_source: JSON.parse(localStorage.getItem('partial_source')) || 'lookbook'
					};
				} else {
					return {
						lead: JSON.parse(localStorage.getItem('lead')),
						pose: JSON.parse(localStorage.getItem('pose')),
						poses: JSON.parse(localStorage.getItem('poses')),
						lookbook: JSON.parse(localStorage.getItem('lookbook')),
						lookbooks: JSON.parse(localStorage.getItem('lookbooks')),
						chictopia: JSON.parse(localStorage.getItem('chictopia')),
						chictopias: JSON.parse(localStorage.getItem('chictopias')),
						status:  ((TagCashBot.getState("lookbook") == "done") &&
								(TagCashBot.getState("pose") == "done") &&
								(TagCashBot.getState("chictopia") == "done")) ? "stopped" : JSON.parse(localStorage.getItem('status')),
						contacts: JSON.parse(localStorage.getItem('contacts')),
						botMode: JSON.parse(localStorage.getItem('botMode')) || false,
						// searchOption: JSON.parse(localStorage.getItem('searchOption')) || 'lookbook',
						maxPage: JSON.parse(localStorage.getItem('maxPage')) || 1,
						partial_lead: JSON.parse(localStorage.getItem('lead')),
						partial_source: JSON.parse(localStorage.getItem('partial_source')) || 'lookbook'
					};
				}
			} else if (["botMode", "status", "partial_lead", "partial_source"].indexOf(key) > -1) {
				return JSON.parse(localStorage.getItem(key));
			} else if (key == "maxPage") {
				return JSON.parse(localStorage.getItem(key)) || "1";
			} else if (key == "contacts") {
				return JSON.parse(localStorage.getItem(key)) || [];
			} else if (key == "bot-contacts") {
				return JSON.parse(localStorage.getItem(key)) || [];
			} else {
				if (TagCashBot.getState("botMode"))
					key += "bot-";
				return JSON.parse(localStorage.getItem(key));
			}
		},

		processLead: function(lead, state, source_option) {
			if (!lead) {
				lead = TagCashBot.getState().lead;
			}

			if (!state) {
				state = TagCashBot.getState();
			}

			if (source_option == "lookbook") {
				TagCashBot.checkLeadOnLookbook(lead.name, state.lookbook);
			} else if (source_option == "pose") {
				TagCashBot.checkLeadOnPose(lead.name, state.pose);
			} else if (source_option == "chictopia") {
				TagCashBot.checkLeadOnChictopia(lead.name, state.chictopia);
			} else if ((!state.lookbook) || (state.lookbook.indexOf('done') == -1)) {
				TagCashBot.checkLeadOnLookbook(lead.name, state.lookbook);
			}

			else if ((!state.pose) || (typeof state.pose != "string" || state.pose.indexOf('done') == -1)) {
				TagCashBot.checkLeadOnPose(lead.name, state.pose);
			}

			else if ((!state.chictopia) || (state.chictopia.indexOf('done') == -1)) {
				TagCashBot.checkLeadOnChictopia(lead.name, state.chictopia);
			} else {
				if (TagCashBot.getState().botMode)
					TagCashBot.nextLead();
				else
					TagCashBot.stop();
			}
		},

		saveState: function(key, value) {
			if (TagCashBot.getState().botMode) {
				if (["botMode", "status", "maxPage", "partial_source", "partial_lead"].indexOf(key) > -1)
					localStorage.setItem(key, JSON.stringify(value));
				else {
					localStorage.setItem("bot-" + key, JSON.stringify(value));
				}
			} else {
				localStorage.setItem(key, JSON.stringify(value));
			}
		},

		nextLead: function() {
			curLead = TagCashBot.getState().lead;
			if (TagCashBot.contact) {
				TagCashAPI.markLeadAsComplete(curLead, TagCashBot.contact, function(res) {
					if (res.status == "ok") {
						TagCashBot.saveState('lookbook', null);
						TagCashBot.saveState('pose', null);
						TagCashBot.saveState('chictopia', null);
						TagCashBot.saveState('lead', null);
						TagCashBot.saveState('source_network', null);
						TagCashBot.start();
						console.log("Searching with new lead...");
					}
				});
			} else {
				TagCashBot.saveState('lookbook', null);
				TagCashBot.saveState('pose', null);
				TagCashBot.saveState('chictopia', null);
				TagCashBot.saveState('lead', null);
				TagCashBot.saveState('source_network', null);
				TagCashBot.start();
			}
		},

		setBotMode: function(mode) {
			TagCashBot.botMode = mode;
			TagCashBot.saveState('botMode', mode);
		},

		avoidExceptionalCase: function(log) {
			console.log("avoidExceptionalCase says: " + log);
			if (TagCashBot.isReady()) {
				TagCashBot.saveContact(function() {
					var curState = TagCashBot.getState();
					if (curState.lookbook && (curState.lookbook != "done")) {
						TagCashBot.processLookbookItem("Blank blog site reference found on Lookbook - " + TagCashBot.source_network_url);
					} else if (curState.pose && (curState.pose != "done")) {
						TagCashBot.processPoseItem("Blank blog site reference found on Pose - " + TagCashBot.source_network_url);
					} else if (curState.chictopia && (curState.chictopia != "done")) {
						TagCashBot.processChictopiaItem("Blank blog site reference found on chictopia - " + TagCashBot.source_network_url);
					} else {
						TagCashBot.stop();
					}
				});
			} else {
				console.log("Unknown issue found");
			}
		},

		closeTabs: function() {
			if (TagCashBot.lookbook_tab_id)
				removeChromeTab(TagCashBot.lookbook_tab_id);

			if (TagCashBot.pose_tab_id)
				removeChromeTab(TagCashBot.pose_tab_id);

			if (TagCashBot.chictopia_tab_id)
				removeChromeTab(TagCashBot.chictopia_tab_id);

			if (TagCashBot.instagram_tab_id)
				removeChromeTab(TagCashBot.instagram_tab_id);

			if (TagCashBot.facebook_tab_id)
				removeChromeTab(TagCashBot.facebook_tab_id);

			if (TagCashBot.twitter_tab_id)
				removeChromeTab(TagCashBot.twitter_tab_id);

			if (TagCashBot.blog_tab_id)
				removeChromeTab(TagCashBot.blog_tab_id);
		},

		stop: function() {
			TagCashBot.timer = null;
			TagCashBot.saveState('status', 'stopped.');
			TagCashBot.closeTabs();
		}
	/*	End of controller methods	*/
};


/**
 *	User defined functions.
 */

	var sendMessage = function(msg, data, callback) {
			if (typeof callback == "function") {
				chrome.extension.sendMessage({
				        msg: msg,
				        data: data
				    }, 
				    callback
				);
			} else {
				chrome.extension.sendMessage({
			        msg: msg,
			        data: data
			    });
			}	
		};

	var removeParams = function(addr) {
			if (!addr) {
				return addr;
			} else if (addr.indexOf("?") > -1) {
				return addr.substring(0, addr.indexOf("?"));
			} else {
				return addr;
			}
		}

	var removeLastSlash = function(addr) {
			if (!addr)
				return addr;

			addr = removeParams(addr);

			if (addr.substr(addr.length - 1) == "/")
				return addr.substring(0, addr.length - 1);
			else
				return addr;
		}

	var removeProtocol = function(addr) {
			var https = "https://",
				http = "http://";

			if (!addr)
				return addr;

			addr = removeLastSlash(addr);
			
			if (addr.indexOf(https) == 0) {
				return addr.substr(https.length);
			} else if (addr.indexOf(http) == 0) {
				return addr.substr(http.length);
			} else {
				return addr;
			}
		};

	var removeWWW = function(addr) {
			var www = "www.";

			if (!addr) {
				return addr;
			} else if (addr.indexOf(www) == 0) {
				return addr.substr(www.length);
			} else {
				return addr;
			}
		};

	var compareUrl = function(url1, url2) {
			url1 = removeWWW(removeProtocol(url1));
			url2 = removeWWW(removeProtocol(url2));

			return (((url1) ? url1.toUpperCase() : null) == ((url2) ? url2.toUpperCase() : null));
		};

	var unique = function(array) {
			return $.grep(array, function(el, index) {
				return index == $.inArray(el, array);
			});
		};

	var removelastAutoWordInPose = function(addr) {
			var suffix = "/poses";
			if (addr.indexOf(suffix) > -1 && (addr.indexOf(suffix) + suffix.length == addr.length))
				return addr.substring(0, addr.indexOf(suffix));
			else
				return addr;
		};

	var comparePoseUrl = function(url1, url2) {
			url1 = removelastAutoWordInPose(removeWWW(removeProtocol(url1)));
			url2 = removelastAutoWordInPose(removeWWW(removeProtocol(url2)));

			return (((url1) ? url1.toUpperCase() : null) == ((url2) ? url2.toUpperCase() : null));
		};

	var isExistInContacts = function(contact, contacts) {
		for (var i = 0; i < contacts.length; i++ ) {
			if (contact.source_network == contacts[i].source_network) {
				return {status: true, index: i};
			}
		}

		return {status: false};
	};

	var refineContacts = function(contacts) {
		var result = [],
			backup = [];

		$.each(contacts, function(i, value) {
			pos = backup.indexOf(value);
			if (pos == -1) {
				result.push(value);
				backup.push(value);
			} else {
				result[pos].note = (result[pos].note || 0) + 1;
			}
		})
		return result;
	};

	var getCSVContent = function(contacts) {
			var contacts = refineContacts(contacts);
			var result = 'Source Network,Email, Website,Instagram profile,Instagram Count,Twitter profile,Twitter Count,Facebook profile,Facebook count,Retailer,Note,Country\n';
			for (var i = 0; i < contacts.length; i++)
			{
				var contact = contacts[i];
				result += '"' + contact.source_network + '"' + ',' + 
						  '"' + (contact.email_address || "") + '"' + ',' +
						  // '"' + (contact.email || "not found") + '"' + ',' +
						  '"' + (contact.blog || "") + '"' + ',' +
						  '"' + contact.instagram + '"' + ',' +
						  '"' + contact.instagram_count + '"' + ',' +
						  '"' + contact.twitter + '"' + ',' +
						  '"' + contact.twitter_count + '"' + ',' +
						  '"' + contact.facebook + '"' + ',' +
						  '"' + contact.facebook_count + '"' + ',' +
						  '"' + contact.retailer + '"' + ',' +
						  '"' + (contact.note ? contact.note : "1") + '"' + ',' +
						  '"' + (contact.country || "not found") + '"' + '\n';
			}
			return result;
		};

	var downloadContacts = function(flag, obj) {
			var contacts = null;

			if (flag == "non-bot")
				contacts = TagCashBot.getState("contacts");
			else
				contacts = TagCashBot.getState("bot-contacts");

			blob = new Blob([getCSVContent(contacts)], { type: 'text/csv' }); //new way
			var csvUrl = URL.createObjectURL(blob);
			obj.attr({
				'href': csvUrl
			});
		};

	var removeChromeTab = function(id, callback) {
			chrome.tabs.query({currentWindow: true}, function(tabs) {
				$.each(tabs, function(i, tab) {
					if (tab.id == id) {
						if (typeof callback == "function")
							chrome.tabs.remove(id, callback);
						else
							chrome.tabs.remove(id);
					}
				});
			});
		};

	var GetEmailsFromString = function(input) {
			var ret = [],
				email = /\"([^\"]+)\"\s+\<([^\>]+)\>/g

			var match;
			while (match = email.exec(input))
				ret.push({'name':match[1], 'email':match[2]})

			return ret;
		};

	var unique = function(array) {
			return $.grep(array, function(el, index) {
				return index == $.inArray(el, array);
			});
		}
	var truncText = function(text, maxLength, ellipseText){
			ellipseText = ellipseText || '&hellip;';

			if (text.length < maxLength) 
			    return text;

			//Find the last piece of string that contain a series of not A-Za-z0-9_ followed by A-Za-z0-9_ starting from maxLength
			var m = text.substr(0, maxLength).match(/([^A-Za-z0-9_]*)[A-Za-z0-9_]*$/);
			if(!m) return ellipseText;

			//Position of last output character
			var lastCharPosition = maxLength-m[0].length;

			//If it is a space or "[" or "(" or "{" then stop one before. 
			if(/[\s\(\[\{]/.test(text[lastCharPosition])) lastCharPosition--;

			//Make sure we do not just return a letter..
			return (lastCharPosition ? text.substr(0, lastCharPosition+1) : '') + ellipseText;
		}
/*	User defined functions	*/